/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpnote.helpers;

import java.awt.Rectangle;
import java.lang.reflect.Field;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

/**
 *
 * @author etienne
 */
public class Helper_Draw {
    
    /**
     * Dessine un visage rectangle
     * @param longueur
     * @param largeur
     * @param gc 
     */
    public static void drawRectVisage(double longueur, double largeur, GraphicsContext gc){
               
        double canvas_h = gc.getCanvas().getHeight();
        double canvas_w = gc.getCanvas().getWidth();

        gc.setStroke(Color.BLACK);
        gc.setLineWidth(3);
        
        // Dessine le rectangle
        gc.strokeRect(canvas_w / 2, canvas_h / 2, longueur, largeur);
    }
    
    /**
     * Dessine un visage rond
     * @param diametre
     * @param gc 
     */
    public static void drawRoundVisage(double diametre, GraphicsContext gc){
           
        double canvas_h = gc.getCanvas().getHeight();
        double canvas_w = gc.getCanvas().getWidth();

        gc.setStroke(Color.BLACK);
        gc.setLineWidth(3);
        
        // Dessine le rectangle
        gc.strokeOval(canvas_w / 2, canvas_h / 2, diametre, diametre);
    }
           
    /**
     * Dessine un visage ovale
     * @param width
     * @param height
     * @param gc 
     */
    public static void drawOvaleVisage(double width, double height, GraphicsContext gc){
           
        double canvas_h = gc.getCanvas().getHeight();
        double canvas_w = gc.getCanvas().getWidth();

        gc.setStroke(Color.BLACK);
        gc.setLineWidth(3);
        
        // Dessine le rectangle
        gc.strokeOval(canvas_w / 2, canvas_h / 2, width, height);
    }
    
    /**
     * Permet de dessiner les yeux avec une couleur donnée
     * @param eyes la couleur des yeux
     * @param gc 
     */
    public static void drawEyes(Color eyes, GraphicsContext gc)
    {
        double canvas_h = gc.getCanvas().getHeight();
        double canvas_w = gc.getCanvas().getWidth();

        // Ajoute les yeux
        gc.setFill(eyes);
        gc.setStroke(Color.BLACK);
        gc.strokeOval(canvas_w / 2 + 30,canvas_h / 2 + 20, 5, 8);
        gc.strokeOval(canvas_w / 2 + 15,canvas_h / 2 + 20, 5, 8);
        gc.fillOval(canvas_w / 2 + 30,canvas_h / 2 + 20, 5, 8);
        gc.fillOval(canvas_w / 2 + 15,canvas_h / 2 + 20, 5, 8);
    }
    
    /**
     * Permet de dessiner les cheveux selon la couleur et la longueur demandé
     * @param hairs
     * @param length
     * @param gc 
     */
    public static void drawHairs(Color hairs, double length, GraphicsContext gc)
    {        
        double canvas_h = gc.getCanvas().getHeight();
        double canvas_w = gc.getCanvas().getWidth();

        // Ajoute les cheveux
        gc.setStroke(hairs);
        gc.strokeLine(canvas_w / 2 + 40,canvas_h / 2 - 0,canvas_w / 2 + 44, canvas_h / 2 - length );
        gc.strokeLine(canvas_w / 2 + 35,canvas_h / 2 - 2,canvas_w / 2 + 37, canvas_h / 2 - length);
        gc.strokeLine(canvas_w / 2 + 30,canvas_h / 2 - 5,canvas_w / 2 + 30, canvas_h / 2 - length);
        gc.strokeLine(canvas_w / 2 + 25,canvas_h / 2 - 5,canvas_w / 2 + 25, canvas_h / 2 - length);
        gc.strokeLine(canvas_w / 2 + 20,canvas_h / 2 - 5,canvas_w / 2 + 20, canvas_h / 2 - length);
        gc.strokeLine(canvas_w / 2 + 15,canvas_h / 2 - 2,canvas_w / 2 + 13, canvas_h / 2 - length);
        gc.strokeLine(canvas_w / 2 + 10,canvas_h / 2 - 0,canvas_w / 2 + 6, canvas_h / 2 - length);

    }

    /**
     * Dessine la bouche de l'avatar en fonction du type de bouche demandé
     * @param shape
     * @param gc 
     */
    public static void drawMouth(String shape, GraphicsContext gc)
    {        
        double canvas_h = gc.getCanvas().getHeight();
        double canvas_w = gc.getCanvas().getWidth();

        gc.setStroke(Color.BLACK);
        gc.setLineWidth(2);

        if(shape.equals("triste"))
        {
            gc.strokeArc(canvas_w / 2 + 15, canvas_h / 2 + 40, 20, 20, 50, 100, ArcType.OPEN);

        }
        else if(shape.equals("sourire"))
        {
            gc.strokeArc(canvas_w / 2 + 15, canvas_h / 2 + 25, 20, 20, 220, 100, ArcType.OPEN);
        }
        else
        {
            gc.strokeLine(canvas_w / 2 + 15, canvas_h / 2 + 40,canvas_w / 2 + 37, canvas_h / 2 + 40);
        }
    }
     
    /**
     * Permet de récupérer la couleur à partir d'une chapine de caractère
     * @param stringColor
     * @return 
     */
    public static Color getColorFromString(String stringColor){
        Color color;
        try {
            Field field = Class.forName("javafx.scene.paint.Color").getField(stringColor.toUpperCase());
           
            color = (Color)field.get(null);
        } catch (Exception e) {
            color = null; // Not defined
            System.out.println("color hasn't been found" + e);
        }
        
        return color;
    }
}
