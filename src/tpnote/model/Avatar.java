/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpnote.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Toggle;
import javafx.scene.paint.Color;

/**
 *
 * @author etienne
 */
public class Avatar {

    private final StringProperty formeVisage = new SimpleStringProperty();
    private final ObjectProperty<Toggle> formeBouche = new SimpleObjectProperty<Toggle>() ;
    private final ObjectProperty<String> couleurYeux = new SimpleObjectProperty();
    private final ObjectProperty<String> couleurCheveux = new SimpleObjectProperty();
    private final DoubleProperty longueurCheveux = new SimpleDoubleProperty();

    public ObjectProperty<String> CouleurYeuxProperty() {
        return couleurYeux;
    }

    public ObjectProperty<String> CouleurCheveuxProperty() {
        return couleurCheveux;
    }

    public DoubleProperty LongueurCheveuxProperty() {
        return longueurCheveux;
    }

    public String getCouleurYeux()
    {
        return couleurYeux.get();
    }
    
    public String getCouleurcheveux()
    {
        return couleurCheveux.get();
    }
    
    public Double getLongueurcheveux()
    {
        return longueurCheveux.get();
    }
        
    public Toggle getFormeBouche() {
        return formeBouche.get();
    }

    public void setFormeBouche(Toggle value) {
        formeBouche.set(value);
    }

    public ObjectProperty<Toggle> formeBoucheProperty() {
        return formeBouche;
    }

    public String getFormeVisage() {
        return formeVisage.get();
    }

    public void setFormeVisage(String value) {
        formeVisage.set(value);
    }

    public StringProperty formeVisageProperty() {
        return formeVisage;
    }
    
    
}
