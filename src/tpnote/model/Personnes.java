/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpnote.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author etienne
 */
public class Personnes {
    
    public ObservableList<Personne> personnes;
    private ObjectProperty<Personne> selectedPersonne = new SimpleObjectProperty<>();

    public Personnes(ObservableList<Personne> personnes){
        this.personnes = FXCollections.observableArrayList(personnes);
    }
    
    public Personnes(){
    
        this.personnes = FXCollections.observableArrayList();
        
        // Create fake data
        personnes.add(new Personne("nom1","prenom1","login1","mdp1","14 ans", "adresse 1"));
        personnes.add(new Personne("nom2","prenom2","login2","mdp2","67 ans", "adresse 2"));
        personnes.add(new Personne("pericat","etienne","etienne","mdpetienne","54 ans", "adresse 3"));
        personnes.add(new Personne("admin","admin","admin","admin","1 ans", "adresse 4"));


    }

    public ObservableList<Personne> getListe() {
        return personnes;
    }
    
    public Personne identification(String login, String password){
        for (Personne p : personnes) {
            
            // Si le mdp/pwd est bon
            if(p.checkPassword(login, password)){
                return p;
            }
        }
        
        return null;
    }
    
    
    public ObjectProperty<Personne> selectedPersonneProperty() {
        return selectedPersonne;
    }   
    
    public Personne getSelectedPersonne() {
        return selectedPersonne.get();
    }
    
    public void setSelectedPersonne(Personne p) {
        selectedPersonne.setValue(p);
    }
}
