/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpnote.model;

import java.time.LocalDateTime;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author etienne
 */
public class Personne {
    
    private StringProperty nom;
    private StringProperty prenom;
    private StringProperty login;
    private StringProperty pwd;
    private StringProperty adresse;
    private StringProperty age;
    
    private Avatar avatar;
    private ObjectProperty<LocalDateTime> lastConnectionDateTime = new SimpleObjectProperty<>();
    private ObjectProperty<Personne> selectedPersonne = new SimpleObjectProperty<>();

    
    public Personne(String nom, String prenom, String login, String mdp, String age, String adresse) {
        this.nom = new SimpleStringProperty(nom);
        this.prenom = new SimpleStringProperty(prenom);
        this.login = new SimpleStringProperty(login);
        this.pwd = new SimpleStringProperty(mdp);
        this.avatar = new Avatar();
        this.age = new SimpleStringProperty(age);
        this.adresse = new SimpleStringProperty(adresse);
    }
    
        public String getAdresse() {
        return adresse.get();
    }

    public void setAdresse(String value) {
        adresse.set(value);
    }

    public StringProperty adresseProperty() {
        return adresse;
    }
    
    public String getAge() {
        return age.get();
    }

    public void setAge(String value) {
        age.set(value);
    }

    public StringProperty ageProperty() {
        return age;
    }

    public Boolean checkPassword(String login, String password){
        return login.equals(this.login.getValue()) && password.equals(this.pwd.getValue());
    }
    
    public StringProperty nomProperty() {
        return nom;
    }
    
    public String getNom()
    {
        return nom.get();
    }

    public String getPrenom()
    {
        return prenom.get();
    }
    
    public void setNom(StringProperty nom) {
        this.nom = nom;
    }

    public StringProperty prenomProperty() {
        return prenom;
    }

    public void setPrenom(StringProperty prenom) {
        this.prenom = prenom;
    }

    public StringProperty loginProperty() {
        return login;
    }

    public String getLogin() {
        return loginProperty().get();
    }

    public StringProperty getMdp() {
        return pwd;
    }

    public void setMdp(StringProperty mdp) {
        this.pwd = mdp;
    }
    
    public LocalDateTime getLastConnectionDateTime() {
        return lastConnectionDateTime.getValue();
    }

    public void setLastConnectionDateTime(LocalDateTime value) {
        lastConnectionDateTime.setValue(value);
    }

    public ObjectProperty lastConnectionDateTimeProperty() {
        return lastConnectionDateTime;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }
    
}
