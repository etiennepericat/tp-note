/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpnote.controllers;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Slider;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.paint.Color;
import tpnote.helpers.Helper_Draw;
import tpnote.model.Caracteristiques;
import tpnote.model.AvatarContext;

/**
 * FXML Controller class
 *
 * @author etienne
 */
public class DrawController implements Initializable {

    @FXML
    private Button bt_reset;
    @FXML
    private ListView<String> lv_yeux;
    @FXML
    private ListView<String> lv_cheveux;
    @FXML
    private Slider sldr_cheveux;
    @FXML
    private Canvas cv_draw;
    @FXML
    private ComboBox<String> cb_visage;
    @FXML
    private Label drawTitle;

    private AvatarContext avatarContexte;
    @FXML
    private RadioButton rb_sourire;
    @FXML
    private RadioButton rb_neutre;
    @FXML
    private RadioButton rb_triste;
    @FXML
    private ToggleGroup bouche;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // ================================
        // == Instanciation des listview ==
        // Même si il y a des bindings actif on ajoute des listener pour automatisé la mise à jour
        // du dessin lors de chacune des modifications
        lv_cheveux.setItems(Caracteristiques.couleurs);
        lv_yeux.setItems(Caracteristiques.couleurs);

        // Personnalisation des cellules en fonction des couleurs
        lv_yeux.setCellFactory((ListView<String> lv) -> new ColoredCell());
        lv_cheveux.setCellFactory((ListView<String> lv) -> new ColoredCell());
        lv_cheveux.getSelectionModel().selectFirst();
        lv_yeux.getSelectionModel().selectFirst();

        // ================================
        // == Instanciation du slider ==
        sldr_cheveux.setMin(0);
        sldr_cheveux.setMax(50);
        sldr_cheveux.setShowTickLabels(true);
        sldr_cheveux.setShowTickMarks(true);
        
        // ==================================
        // == Instanciation de la combobox ==
        cb_visage.setItems(Caracteristiques.formes);
        cb_visage.getSelectionModel().selectFirst();

        // ==========================================
        // == Groupe de radioBouttons de la bouche ==
        rb_neutre.setUserData("neutre");
        rb_sourire.setUserData("sourire");
        rb_triste.setUserData("triste");
        bouche.selectToggle(rb_neutre);


        // ================================
        // == Bouton de réinitialisation ==
        bt_reset.setOnAction((event) -> {

            // Déselectionne toutes les entrées
            lv_cheveux.getSelectionModel().clearSelection();
            lv_yeux.getSelectionModel().clearSelection();
            cb_visage.getSelectionModel().clearSelection();
            sldr_cheveux.valueProperty().set(0);
            bouche.selectToggle(rb_neutre);
            
            // Clean le canvas
            updateAvatar();
        });
    }

    /**
     * Listener appelé lorsque la valeur du slider a été modifié.
     */
    private ChangeListener<Number> sldr_changeListener = new ChangeListener<Number>() {

        @Override
        public void changed(ObservableValue<? extends Number> ov, Number old, Number newN) {
            // Update draw
            updateAvatar();
        }
    };

    /**
     * Listener appelé lorsque la valeur de la listeView sélectionné a été
     * modifié
     */
    private ChangeListener<String> lv_itemChangedListener = new ChangeListener<String>() {

        @Override
        public void changed(ObservableValue<? extends String> ov, String old, String newS) {
            // Perform update draw
            updateAvatar();
        }
    };

    /* Listener appelé lorsque la valeur de la combobox sélectionné a été modifié
     */
    private ChangeListener<String> cb_itemChangedListener = new ChangeListener<String>() {

        @Override
        public void changed(ObservableValue<? extends String> ov, String old, String newN) {
            // Update draw
            updateAvatar();
        }
    };

    /**
     * Permet de redessiner l'avatar à partir de toutes les caractéristiques
     */
    private void updateAvatar() {
        // Récupère la forme a afficher 
        if (getAvatarContexte() != null) {
            GraphicsContext gc = cv_draw.getGraphicsContext2D();
            gc.clearRect(0, 0, cv_draw.getWidth(), cv_draw.getHeight());

            String formeVisage = getAvatarContexte().getConnectedUser().getAvatar().getFormeVisage();

            // #------------------- #
            // # Dessine le visage  #
            // #------------------- #
            if (formeVisage == "rond") {
                Helper_Draw.drawRoundVisage(50, gc);
            } else if (formeVisage == "ovale") {
                Helper_Draw.drawOvaleVisage(50, 70, gc);
            } else // carré ou non sélectionné
            {
                Helper_Draw.drawRectVisage(50, 50, gc);
            }

            // #------------------ ---#
            // # Dessine les cheveux  #
            // #--------------------- #
            Color selectedHairsColor = Helper_Draw.getColorFromString(getAvatarContexte().getConnectedUser().getAvatar().getCouleurcheveux());
            if (selectedHairsColor == null) {
                selectedHairsColor = Color.CADETBLUE;
            }
            double longueur = sldr_cheveux.getValue();

            if (longueur > 0) {
                Helper_Draw.drawHairs(selectedHairsColor, longueur, gc);
            }

            // #------------------ #
            // # Dessine les yeux  #
            // #------------------ #
            Color selectedColor = Helper_Draw.getColorFromString(getAvatarContexte().getConnectedUser().getAvatar().getCouleurYeux());
            if (selectedColor == null) {
                selectedColor = Color.CADETBLUE;
            }
            Helper_Draw.drawEyes(selectedColor, gc);

            // #------------------ #
            // # Dessine la bouche #
            // #------------------ #
            String bouche = getAvatarContexte().getConnectedUser().getAvatar().getFormeBouche().getUserData().toString();
            Helper_Draw.drawMouth(bouche, gc);
        }
    }

    /**
     * Cette classe permet de personnaliser le style des lignes présentes dans
     * les listview.
     *
     * Elle permet d'assigner à partir du nom de la couleur la bonne couleur du
     * texte à afficher
     */
    private class ColoredCell extends ListCell<String> {

        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            // Si l'item n'est pas nul
            if (item != null) {

                // Fixe la couleur à partir du nom
                setTextFill(Color.web(item));

                // Fixe le contenu à afficher
                setText(item);
            }
        }
    }

    public AvatarContext getAvatarContexte() {
        return this.avatarContexte;
    }

    public void setAvatarContexte(AvatarContext avatarContexte) {
        this.avatarContexte = avatarContexte;

        if (avatarContexte.getConnectedUser().prenomProperty() != null && avatarContexte.getConnectedUser().nomProperty() != null) {
            // Ajoute le titre
            //drawTitle.setText(getAvatarContexte().getP().getPrenom().get().toString() + " " + getAvatarContexte().getP().getNom().get().toString());
            drawTitle.textProperty().bind(getAvatarContexte().getConnectedUser().prenomProperty());
        }

        // Bindings (pour mettre à jour automatiquement les valeurs dans l'objet model)
        avatarContexte.getConnectedUser().getAvatar().formeVisageProperty().bind(cb_visage.selectionModelProperty().get().selectedItemProperty());
        avatarContexte.getConnectedUser().getAvatar().formeBoucheProperty().bind(bouche.selectedToggleProperty());
        avatarContexte.getConnectedUser().getAvatar().CouleurCheveuxProperty().bind(lv_cheveux.getSelectionModel().selectedItemProperty());
        avatarContexte.getConnectedUser().getAvatar().CouleurYeuxProperty().bind(lv_yeux.getSelectionModel().selectedItemProperty());
        avatarContexte.getConnectedUser().getAvatar().LongueurCheveuxProperty().bindBidirectional(sldr_cheveux.valueProperty());
        
        // Ajout des évènement de sélection 
        getAvatarContexte().getConnectedUser().getAvatar().CouleurYeuxProperty().addListener(lv_itemChangedListener);
        getAvatarContexte().getConnectedUser().getAvatar().CouleurCheveuxProperty().addListener(lv_itemChangedListener);
        getAvatarContexte().getConnectedUser().getAvatar().LongueurCheveuxProperty().addListener(sldr_changeListener);
        getAvatarContexte().getConnectedUser().getAvatar().formeVisageProperty().addListener(cb_itemChangedListener);
        getAvatarContexte().getConnectedUser().getAvatar().formeBoucheProperty().addListener((event) -> {
            updateAvatar(); // Mise à jour du dessin lors de la sélection
        });
        
        // Premier dessin
        updateAvatar();
    }
}
