/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpnote.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import tpnote.TpNote;
import tpnote.model.AvatarContext;
import tpnote.model.Personne;
import tpnote.model.Personnes;

/**
 * FXML Controller class
 *
 * @author etienne
 */
public class HomeController implements Initializable {

    @FXML
    private MenuItem mi_profile;
    @FXML
    private MenuItem mi_avatar;
    @FXML
    private MenuItem mi_quitter;

    private AvatarContext avatarContext;
    
    private static String drawView_filepath = "/tpnote/views/Draw.fxml";
    @FXML
    private TableView<Personne> table_users;
    @FXML
    private TableColumn<Personne, String> cl_login;
    @FXML
    private TableColumn<Personne, String> cl_nom;
    @FXML
    private TableColumn<Personne, String> cl_adresse;
    @FXML
    private TextField tf_login;
    @FXML
    private TextField tf_nom;
    @FXML
    private TextField tf_adresse;
    @FXML
    private Button bt_ajouter;
    @FXML
    private Button bt_supprimer;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        mi_avatar.setOnAction((event) -> 
        {
            try {
                // Charge la vue
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(TpNote.class.getResource(drawView_filepath));
                BorderPane root = (BorderPane) loader.load();
                
                DrawController drawControler = loader.getController();
                drawControler.setAvatarContexte(avatarContext);
                
                // Redirection vers le HOME
                Stage stage = new Stage();
                Scene draw_scene = new Scene(root);
                stage.setScene(draw_scene);
                stage.show();
                
            } catch (IOException ex) {
                Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        mi_quitter.setOnAction((event) -> 
        {
            System.exit(0);
        });
        
        mi_profile.setOnAction((event) ->
        {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Profile");
            alert.setHeaderText(getAvatarContext().getConnectedUser().getNom() + " " + getAvatarContext().getConnectedUser().getPrenom());
            alert.setContentText(String.format("Age : %s\nAdresse : %s", getAvatarContext().getConnectedUser().getAge(), getAvatarContext().getConnectedUser().getAdresse()));

            alert.showAndWait();
        });       
        
        bt_ajouter.setOnAction((t) -> {
           
            // Ajout de la personne dans la liste
            Personne newP = new Personne(tf_nom.getText() , "",tf_login.getText(), "","", tf_adresse.getText());
            getAvatarContext().getPersonnes().getListe().add(newP);
        });
        
        bt_ajouter.disableProperty()
                .bind(tf_login.textProperty().isEmpty()
                .or(tf_adresse.textProperty().isEmpty()
                .or(tf_nom.textProperty().isEmpty())));
        
        bt_supprimer.disableProperty().bind(table_users.getSelectionModel().selectedItemProperty().isNull());
        
        bt_supprimer.setOnAction((t) -> {
            
            getAvatarContext().getPersonnes().getListe().remove(getAvatarContext().getPersonnes().getSelectedPersonne());
        });
        
        cl_nom.setEditable(true);
        cl_nom.setCellValueFactory(new PropertyValueFactory<Personne, String>("nom"));
        cl_nom.setCellFactory(TextFieldTableCell.forTableColumn());

    }
        
    private void lineSelected(Personne p){
        
        Platform.runLater(new Runnable() {
            @Override
            public void run() {

                tf_login.setText(p.getLogin()) ;
                tf_nom.setText(p.getNom());
                tf_adresse.setText(p.getAdresse());
            }
        });
                
    }    

    void setAvatarContexte(AvatarContext avatarContext) {
        this.avatarContext = avatarContext;

        // Remplissage tu tableau utilisateurs
        cl_login.setCellValueFactory(new PropertyValueFactory<Personne, String>("login"));
        cl_login.setCellFactory((column) -> {
            return new TableCell<Personne, String>(){
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty); 
                    
                    if(item != null && !empty)
                    {
                        setText(item);
                        setStyle("-fx-text-fill: red;-fx-font-weight: bold;; -fx-background-color: blue;");
                    }
                }
                
            };
        });
        
        cl_adresse.setCellValueFactory(new PropertyValueFactory<Personne, String>("adresse"));
        table_users.setItems(getAvatarContext().getPersonnes().getListe());
        
        table_users.getSelectionModel().selectedItemProperty().addListener((ov, oldPersonne, newPersonne) -> {
            lineSelected(newPersonne);
        });
        
        getAvatarContext().getPersonnes().selectedPersonneProperty().bind(table_users.getSelectionModel().selectedItemProperty());

    }

    public AvatarContext getAvatarContext() {
        return avatarContext;
    }
    
}
