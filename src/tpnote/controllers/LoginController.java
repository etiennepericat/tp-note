/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpnote.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import tpnote.TpNote;
import tpnote.model.LoginContexte;
import tpnote.model.Personne;
import tpnote.model.AvatarContext;
import tpnote.model.Personnes;
/**
 * FXML Controller class
 *
 * @author etienne
 */
public class LoginController implements Initializable {

    @FXML
    private Button okButton;
    @FXML
    private Button cancelButton;
    @FXML
    private TextField loginField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private ButtonBar buttonBar;
    
    private LoginContexte loginContexte;
    private String HomeView_filePath = "/tpnote/views/Home.fxml";
    
    @FXML
    private Label passwordVerif;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                // Demande le focus 
                loginField.requestFocus();
            }
        });
        
        // Fixe l'ordre des boutons dans la bouton bar
       ButtonBar.setButtonData(okButton, ButtonBar.ButtonData.OK_DONE);
       ButtonBar.setButtonData(cancelButton, ButtonBar.ButtonData.CANCEL_CLOSE);

    }

    public LoginContexte getLoginContexte() {
        return loginContexte;
    }

    public void setLoginContexte(LoginContexte loginContexte) {
        this.loginContexte = loginContexte;
        
        // Effectue les binding entre contexte et vue
        loginContexte.getLoginUtilisateurConnecteProperty().bind(loginField.textProperty());
        loginContexte.getPasswordUtilisateurConnecteProperty().bind(passwordField.textProperty());
        
        passwordField.textProperty().addListener(new ChangeListener<String>() {
            
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String newS) {
                
                String message = "";

                if(newS != null && newS.length() >= 0)
                {
                    // Si il manque un chiffre
                    if(!newS.matches(".*\\d+.*"))
                    {
                        message = "Il manque un chiffre.";
                    }
                    
                    // Contient un caract spécial
                    Pattern p = Pattern.compile("[^a-zA-Z0-9]");
                    if(!p.matcher(newS).find())
                    {
                        message += "Il manque un caractère spécial.";
                    }
                }
                
                passwordVerif.setText(message);
            }
        });
        
        // Relie l'activation du bouton aux autre propriétés
        okButton.disableProperty().bind(loginField.textProperty().isEmpty().or(passwordField.textProperty().isEmpty()));
    }
    
    @FXML
    private void processOk(ActionEvent event) {
        
        Personne connectedUser = loginContexte.identification();
        
        if(connectedUser != null)
        {
            System.out.println("The user " + loginField.getText() + " has been connected successfully.");
            
            try {
                // Charge la vue 
               FXMLLoader loader = new FXMLLoader();
               loader.setLocation(TpNote.class.getResource(HomeView_filePath));
               BorderPane root = (BorderPane) loader.load();

               HomeController homeController = loader.getController();
               homeController.setAvatarContexte(new AvatarContext(connectedUser, new Personnes()));
               
               // Redirection vers le HOME 
               Stage stage = (Stage) this.loginField.getScene().getWindow();
               Scene draw_scene = new Scene(root);
               stage.setScene(draw_scene);
               
            } catch (IOException ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        else
        {
            System.out.println("The user " + loginField.getText() + " has not been connected.");
        }
    }

    @FXML
    private void processCancel(ActionEvent event) {
        loginField.setText("");
        passwordField.setText("");
    }


    @FXML
    private void onMouseExited(MouseEvent event) {
                loginField.setStyle("-fx-control-inner-background: white;");
    }

    @FXML
    private void onMouseEntered(MouseEvent event) {
                loginField.setStyle("-fx-control-inner-background: red;");
    }
}
