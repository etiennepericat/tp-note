Groupe : Kaïs Ben-Youssef et Etienne Pericat

Nous avons réalisé l'ensemble des fonctionnalités demandées : 
    - Choix de la forme du visage
    - Choix de la longueur et couleur des cheveux
    - Choix de la couleur des yeux
    - Choix de la forme de la bouche
    - Remise à zero des paramètres

    -- bindings pour enregistrer les valeurs sur l'objet du model 
    -- listener pour mettre à jour automatiquement le dessin de l'avatar
    -- passage du contexte d'un controller à l'autre (pour afficher le nom de l'utilisateur connecté)

